<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        require_once('Animal.php');
        class Ape extends Animal {
            public $leg = 2;
            public function yell() {
                echo "Yell : Auooo";
            }
        }
    ?>
    
</body>
</html>