

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        require('Animal.php');

        $sheep = new Animal("shaun");
        echo "Name :" . $sheep->get_name() . "<br>"; // "shaun"
        echo "Legs :" . $sheep->get_legs() . "<br>"; // 4
        echo "Cold Blooded :" . $sheep->get_cold_blooded() . "<br>" . "<br>"; // "no"

        $kodoknew = new Animal("buduk");
        echo "Name :" . $kodoknew->get_name() . "<br>"; // "shaun"
        echo "Legs :" . $kodoknew->get_legs() . "<br>"; // 4
        echo "Cold Blooded :" . $sheep->get_cold_blooded() . "<br>";
        require_once('Frog.php');
        $kodok = new Frog("buduk");
        $kodok->jump(); // "hop hop"
        
        $buduknew = new Animal("buduk");
        echo "Name :" . $buduknew->get_name() . "<br>"; // "shaun"
        require_once('Ape.php');
        $sungokong = new Ape("kera sakti");
        echo "Legs : " .$sungokong->leg . "<br>";
        echo "Cold Blooded :" . $buduknew->get_cold_blooded() . "<br>";
        $sungokong->yell(); // "Auooo"

        
    ?>
</body>
</html>