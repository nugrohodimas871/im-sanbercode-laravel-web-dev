<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>

    <form action="/kirim" method="post">
    @csrf
        <label for="fname">First Name :</label><br>
        <input type="text" name="fname" id="fname"><br>
        <label for="lname">Last Name :</label><br>
        <input type="text" name="lname" id="lname"><br>

        <br>
        <br>
        
        <label for="gender">Gender</label><br>
        <input type="radio" id="men" name="gender_option" value="Men">
        <label for="men">Men</label><br>
        <input type="radio" id="women" name="gender_option" value="Women">
        <label for="women">Women</label><br>
        <input type="radio" id="other" name="gender_option" value="Other">
        <label for="other">Other</label>

        <br>
        <br>

        
        <label for="national">Nationality :</label>
        <select name="national" id="national">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
        </select>

        <br>
        <br>

        <label>Languge Spoken:</label><br>
        <input type="checkbox" id="language1" name="language1" value="Bahasa Indonesia">
        <label for="language1"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="language2" name="language2" value="English">
        <label for="language2"> English</label><br>
        <input type="checkbox" id="language3" name="language3" value="Arabic">
        <label for="language3"> Arabic </label><br>
        <input type="checkbox" id="language4" name="language4" value="Japanese">
        <label for="language4"> Japanese </label>

        <br>
        <br>

        <label for="bio"></label><br>
        <textarea name="bio" rows="10" cols="30"></textarea>

        <br>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>