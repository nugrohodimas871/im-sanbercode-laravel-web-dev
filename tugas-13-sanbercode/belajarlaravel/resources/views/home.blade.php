@extends('layout.master');

@section('judul')
    SanberBook
@endsection

@section('content')
    <h3>Social Media Developer </h3>
    <p>Belajar dan Berbagi agar hidup ini semakin santai dan berkualitas</p>

    <h3>Benefit Join SanberBook</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama Developer</li>
        <li>Sharing knowledge dari para mentor</li>
        <li>Dibuat oleh calon web Developer Terbaik</li>
    </ul>

    <h3>Cara Bergabung ke SanberBook</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftar di <a href = "/register">Form Sign Up </a></li>
        <li>Selesai</li>
    </ol>
@endsection
