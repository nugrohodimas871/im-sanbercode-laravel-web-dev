<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis(){
        return view ('register');
    }
    public function newuser(){
        return view ('home');
    }
    public function kirim(Request $request){
        
        $namadepan = $request['fname'];
        $namabelakang = $request['lname'];

        return view('welcome', ['namadepan' => $namadepan, 'namabelakang' => $namabelakang]);
    }
}
