<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\FormKontroller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/register', [RegisterController::class, 'register'])->name('register');
Route::post('/kirim', [App\Http\Controllers\RegisterController::class, 'actionregister'])->name('kirim');

Route::get('/', [LoginController::class, 'login'])->name('login');
Route::post('/actionlogin', [LoginController::class, 'actionlogin'])->name('actionlogin');

Route::get('/master', [HomeController::class, 'master'])->name('master')->middleware('auth');
Route::get('actionlogout', [LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');

Route::get('/login', function () {
    return view('login');
});
Route::get('/master', function () {
    return view('master     ');
});
Route::get('/table', function ()
{
    return view('table');
});


//CRUD 
// Create
//tambah form
Route::get('/form',[FormKontroller::class, 'create']);
//kirim data ke database
Route::post('/data', [FormKontroller::class, 'add']);


//Read Database
Route::get('/data', [FormKontroller::class, 'index']);
//Update Database
Route::get('/edit/{form_id}', [FormKontroller::class, 'edit']);
//Update Ke DataBase Berdasarkan ID
Route::put('/edit/{form_id}', [FormKontroller::class, 'update']);
//Delete Berdasarkan ID
Route::delete('/edit/{form_id}', [FormKontroller::class, 'destroy']);