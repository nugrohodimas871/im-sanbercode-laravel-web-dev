@extends('master')

@section('judul')
    <h1>Data</h1>
@endsection

@section('content')

<table class="table table-bordered">
    <thead>                  
      <tr>
        <th style="width: 10px">No</th>
        <th>Nama Peminjam</th>
        <th>Buku Yang dipinjam</th>
        <th >Waktu peminjaman</th>
        <th>Waktu pengembalian</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($data as $key=>$value)
      <tr>
        <td>{{$key + 1}}</td>
        <td>{{$value->NamaPeminjam}}</td>
        <td>{{$value->BukuYangDipinjam}}</td>
        <td>{{$value->WaktuPeminjaman}}</td>
        <td>{{$value->WaktuPengembalian}}</td>
        <td>
          <form action="/edit/{{$value->id}} " method="POST">
            @csrf
            @method('DELETE')
            <a href="/edit/{{$value->id}}" class="btn btn-primary btn-sm">Edit</a>
            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
          </form>
        </td>
      </tr>
    @empty
    @endforelse
    </tbody>
  </table>
  <a href="/form" class="btn btn-primary btn-s">Tambah Data</a>
  <br>   
@endsection