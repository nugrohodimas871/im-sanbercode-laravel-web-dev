@extends('master')

@section('judul')
    <h1>Data</h1>
@endsection

@section('content')

<table class="table table-bordered">
    <thead>                  
      <tr>
        <th style="width: 10px">No</th>
        <th>Nama Peminjam</th>
        <th>Buku Yang dipinjam</th>
        <th >Waktu peminjaman</th>
        <th>Waktu pengembalian</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        @include('tempatTable')
      </tr>
    </tbody>
  </table>
  <br>   
@endsection