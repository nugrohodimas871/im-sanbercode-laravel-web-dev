@extends('master')

@section('Judul')
Halama Tambah Data Form    
@endsection

@section('content')

<form action ='/data' method = 'post'>
    @csrf
    <div class="form-group">
        <label>Nama Peminjam</label>
        <input type="text" class="form-control" name="NamaPeminjam" placeholder="Enter email"> <br>
    </div>
    @error('NamaPeminjam')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Judul Buku</label>
        <input type="text" class="form-control" name="BukuYangDipinjam" placeholder="Masukan Judul Buku"> <br>
    </div>
    @error('BukuYangDipinjam')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Waktu Peminjaman</label>
        <input type="text" class="form-control" name="WaktuPeminjaman" placeholder="Masukan Tanggal Peminjaman"> <br>
    </div>
    @error('WaktuPeminjaman')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Waktu Pengembalian</label>
        <input type="text" class="form-control" name="WaktuPengembalian" placeholder="Masukan Tanggal Pengembalian"> <br>
    </div>
    @error('WaktuPengembalian')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
    
</form>
    
@endsection