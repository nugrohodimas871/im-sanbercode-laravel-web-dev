<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormKontroller extends Controller
{
    public function create()
    {
        return view('form');
    }

    public function add(Request $request)
    {
        $request->validate([
            'NamaPeminjam'=>'required',
            'BukuYangDipinjam'=>'required',
            'WaktuPeminjaman'=>'required',
            'WaktuPengembalian'=>'required'
        ]);

        DB::table('form')->insert([
            'NamaPeminjam'=>$request['NamaPeminjam'],
            'BukuYangDipinjam'=>$request['BukuYangDipinjam'],
            'WaktuPeminjaman'=>$request['WaktuPeminjaman'],
            'WaktuPengembalian'=>$request['WaktuPengembalian']
        ]);
        
        return redirect('data');
    }

    public function data()
    {
        return view('data');
    }

    public function index()
    {
        $data = DB::table('form')->get();
        return view('tambahData', ['data' => $data]);
    }


    public function edit($id)
    {
        $data = DB::table('form')->where('id', $id)->first();
        return view('edit', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'NamaPeminjam'=>'required',
            'BukuYangDipinjam'=>'required',
            'WaktuPeminjaman'=>'required',
            'WaktuPengembalian'=>'required'
        ]);

        DB::table('form')
              ->where('id', $id)
              ->update(
                [
                    'NamaPeminjam'=>$request->NamaPeminjam,
                    'BukuYangDipinjam'=>$request->BukuYangDipinjam,
                    'WaktuPeminjaman'=>$request->WaktuPeminjaman,
                    'WaktuPengembalian'=>$request->WaktuPengembalian
                ]
            );
        return redirect('/data');
    }

    public function destroy($id)
    {
        DB::table('form')->where('id', $id)->delete();
        return redirect('/data');

    }
}